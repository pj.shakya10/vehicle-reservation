<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/',[App\Http\Controllers\WelcomePageController::class, 'index']);
Auth::routes();

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('reservation',[App\Http\Controllers\ReservationController::class, 'index'])->middleware('auth');
Route::post('reserve',[App\Http\Controllers\ReservationController::class, 'store'])->middleware('auth');
Route::delete('reservation/delete/{id}',[App\Http\Controllers\ReservationController::class, 'destroy']);

Route::get('adminaddvehicle',[App\Http\Controllers\AdminController::class, 'index'])->middleware('admin');
Route::post('adminaddVehicle',[App\Http\Controllers\AdminController::class, 'store']);

Route::get('admininventory',[App\Http\Controllers\AdminInventoryController::class, 'index'])->middleware('admin');
Route::post('admininventoryupdate/{id}',[App\Http\Controllers\AdminInventoryController::class, 'update']);
Route::delete('admininventory/delete/{id}',[App\Http\Controllers\AdminInventoryController::class, 'destroy']);

Route::get('adminreservation',[App\Http\Controllers\AdminReservationController::class, 'index'])->middleware('admin');
Route::post('adminreservation/update/{id}',[App\Http\Controllers\AdminReservationController::class, 'update']);
Route::delete('adminreservation/delete/{id}',[App\Http\Controllers\AdminReservationController::class, 'destroy']);
Route::delete('adminreservation/deleteuser/{id}',[App\Http\Controllers\AdminReservationController::class, 'destroyUser']);

Route::get('adminexpense',[App\Http\Controllers\AdminExpenseController::class, 'index'])->middleware('admin');
Route::post('adminexpense/add',[App\Http\Controllers\AdminExpenseController::class, 'store']);
Route::post('adminexpense/delete/{id}',[App\Http\Controllers\AdminExpenseController::class, 'destroy']);
Route::post('adminexpense/update/{id}',[App\Http\Controllers\AdminExpenseController::class, 'update']);

Route::get('payment-verify/{id}',[App\Http\Controllers\EsewaController::class, 'verifyPayment']);

Route::get('adminincome',[App\Http\Controllers\AdminIncomeController::class, 'index'])->middleware('admin');
Route::post('adminincome/add',[App\Http\Controllers\AdminIncomeController::class, 'store']);
Route::post('adminincome/delete/{id}',[App\Http\Controllers\AdminIncomeController::class, 'destroy']);
Route::post('adminincome/update/{id}',[App\Http\Controllers\AdminIncomeController::class, 'update']);