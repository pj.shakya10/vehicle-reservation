@extends('layouts.app')
@section('content')
<div class="container">
    
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6">
        @if (session('message')=='Vehicle added successfully')
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if (session('message')=='This vehicle has already been added')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
            
        <div class="col-lg-6 md-6">
            <div class="card">
                <div class="card-header">Add Vehicle</div>
                    <div class="card-body">  
                        <form action="/adminaddVehicle" method="POST" enctype="multipart/form-data">
                            @csrf
                                                     
                            <div class="mb-2">
                                <input type="text" class="form-control"  name="vehicle_name" value="{{ old('vehicle_name') }}" placeholder="Vehicle name">
                                <span style="color:red">
                                    @error('vehicle_name')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>
                            <div class="mb-2">
                                <select class="form-select" name="vehicle_category">
                                    <option selected disabled>Select Vehicle Category</option>
                                    <option value="Hatchback">Hatchback</option>
                                    <option value="Suv">Suv</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="Bus">Bus</option>
                                    <option value="MiniBus">Minibus</option>
                                </select>
                                <span style="color:red">
                                    @error('vehicle_category')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>
                           
                            <div class="mb-2">
                                <input type="number" class="form-control"  name="occupants" value="{{ old('occupants') }}" placeholder="Occupants(including Driver)">
                                <span style="color:red">
                                    @error('occupants')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="mb-2 mr-2">
                                    <input type="number" class="form-control"  name="base_price" value="{{ old('base_price') }}" placeholder="Base price">
                                    <span style="color:red">
                                        @error('base_price')
                                        {{$message}}
                                        @enderror
                                    </span>
                                </div>

                                <div class="mb-2 mr-2">
                                    <input type="number" class="form-control"  name="per_km_price" value="{{ old('per_km_price') }}" placeholder="Per Km price">
                                    <span style="color:red">
                                        @error('per_km_price')
                                        {{$message}}
                                        @enderror
                                    </span>
                                </div>
                            </div>

                            <label for="formFile" class="form-label" >Vehicle photo</label>
                            <div class="d-flex flex-row">
                            <div class="mb-2">
                                
                                <input class="form-control" type="file" name="vehicle_photo">
                                <span style="color:red">
                                    @error('vehicle_photo')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>

                            <div class="mb-2">
                            <select class="form-select" name="vehicle_status">
                                <option selected disabled>Select Vehicle Status</option>
                                <option value="Active">Active</option>
                                <option value="Deactive">Deactive</option>  
                            </select>
                            <span style="color:red">
                                    @error('status')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>
                            </div>

                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <button class="btn btn-primary" type="submit">Add</button>
                                <button class="btn btn-secondary" type="reset" onclick="window.location.reload()">Clear</button>
                            </div>
                        </form>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection