<?php
namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers;

use App\Models\Income;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

class AdminIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Income::all();
        $total=0;
        $recent=0;
        foreach($data as $d){
            $total=$d->sum('amount');
            $recent=$d->amount;
        }
        $data=Income::latest()->paginate(3);     
        return view('adminincome',compact('data','total','recent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required', 'string', 'max:255',
            'date' => 'required',
            'income_type' => 'required',
            'amount' => 'required',
        ]);
        // dd($request);
        $data=new Income();
        $data->description=$request->description;
        $data->date=$request->date;
        $data->income_type=$request->income_type;
        $data->amount=$request->amount;
        $data->save();
        Session::flash('message', 'Income added.');
        return redirect('adminincome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Income::findOrfail($id);
        
        $data->description=$request->description;
        $data->date=$request->date;
        $data->income_type=$request->income_type;
        $data->amount=$request->amount;
        $data->update();
        Session::flash('message', 'Income updated.');
        return redirect('adminincome');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Income::findOrfail($id);
        // return $data;      
        $data->delete();
        
        return redirect('/adminincome');
    }
}
