<html>

<head>
    <title>Dynamic Vehicle Solution</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>

    <div id="app">
        <div class="background"></div>
        <div class="content-content">
            <div class="content-body">
                <h1> Your Journey Companion !!!</h1>
            </div>
        </div>
        @include('partials.nav')

        <div class="breakline"></div>
        <div class="services">
            <div class="container">
                <h1 style="text-align: center;"><strong>What services do we offer to our clients</strong></h1>
                <div class="row p-2 mt-5" style="text-align: center;">
                    <div class="col-6">
                        <div class="h-100 shadow-lg p-3 mb-3" style="border-radius:25px; background-color:rgb(254,254,254)">
                            <div class="d-flex flex-column justify-content-evenly" style="align-items:center">
                                <i class="fas fa-user-tie"></i>
                                <h2>Professionalism</h2>
                                <p>Professional and experienced drivers for every journey.</p>
                            </div>
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="h-100 shadow-lg p-3 mb-3" style="border-radius:25px; background-color:rgb(254,254,254)">
                            <div class="d-flex flex-column justify-content-evenly" style="align-items:center">
                                <i class="fas fa-handshake"></i>
                                <h2>Reliable service</h2>
                                <p>Well-maintained transport, reliable service.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-2 mt-3" style="text-align: center;">
                    <div class="col-6">
                        <div class="h-100 shadow-lg p-3 mb-3" style="border-radius:25px; background-color:rgb(254,254,254)">
                            <div class="d-flex flex-column justify-content-evenly" style="align-items:center">
                                <i class="fas fa-user-shield"></i>
                                <h2>Secure</h2>
                                <p>Client safety is top priority.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="h-100 shadow-lg p-3 mb-3" style="border-radius:25px; background-color:rgb(254,254,254)">
                            <div class="d-flex flex-column justify-content-evenly" style="align-items:center">
                                <i class="fas fa-headset"></i>
                                <h2>Great support</h2>
                                <p>We, welcome all customer request and provide professional support.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="breakline"></div>
        <div class="gallery">
            <div class="container-fluid">
                <!-- carousel starts -->
                <div id="carouselExampleFade" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($data as $item)
                        <div class="carousel-item {{$loop->index==0?'active':''}}">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 sm-6">
                                        <div class="vehicleinfo shadow-lg" style="border-radius:25px">
                                            <div class="d-flex flex-column" style="align-items:center">
                                                <h2><strong>{{$item->vehicle_name}}</strong></h2>
                                                <p>Occupants(including driver): <strong>{{$item->occupants}}</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 sm-6">

                                        <img src="{{$item->vehicle_photo}}" class="d-block w-100" alt="{{$item->id}}" height="650vh">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <!-- caraousel ends -->

            </div>
        </div>
        <div class="contactinfo">
            <div class="container">
                <div class="row p-5" style="color:white">
                    <div class="col-md-8">

                        <div class="d-flex flex-column">
                            <h4>Contact Information</h4>
                            <p mt-10>Jamsikhel, Lalitpur-3</p>
                            <p mt-8>Email:info@dynamicvehiclesolution.com</p>
                            <p mt-8>Tel: 01-5555999</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="d-flex flex-row">
                            <h4>Get Connected To Us</h4>
                        </div>
                        <div class="d-flex flex-row  mt-3">
                            <a href="">
                                <i class="fab fa-facebook-square fa-3x m-2"></i>
                            </a>

                            <a href="">
                                <i class="fab fa-instagram-square fa-3x m-2"></i>
                            </a>

                            <a href="">
                                <i class="fab fa-twitter-square fa-3x m-2"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>