<?php
namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class AdminInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data=Vehicle::paginate(4);
        // return $data;
        return view('admininventory',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $vehicle=Vehicle::findOrfail($id);
        //return $vehicle;
        $photo=$request->vehicle_photo;
        if(!$photo==null)
        {   
            try{
                unlink($vehicle->vehicle_photo);
            }catch(\Exception $e){}
            $fileName = time().'.'.$request->vehicle_photo->extension();  
            $request->vehicle_photo->move(public_path('uploads'), $fileName);
            $vehicle->vehicle_photo="uploads\\\\".$fileName;
        }
        $vehicle->vehicle_name=$request->vehicle_name;
        $vehicle->vehicle_category=$request->vehicle_category;
        $vehicle->base_price=$request->base_price;
        $vehicle->per_km_price=$request->per_km_price;
        $vehicle->occupants=$request->occupants;
        $vehicle->status=$request->vehicle_status;
        $vehicle->update();
        Session::flash('message','Vehicle edited successfully'); 
        return redirect('admininventory');
        
        
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Vehicle::findOrfail($id);
    
        try{
            unlink($data->vehicle_photo);
        }catch(\Exception $e){}
        $data->delete();
        
        return redirect('/admininventory');
    }
}
