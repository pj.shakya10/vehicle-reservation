<nav class="navbar navbar-expand-md  shadow-sm">
    <div class="container">
        @if (Auth::check())
        <a class="navbar-brand" style="background:red;height:100%;padding: 5px; border-radius:2vh" href="{{ url('/home') }}">
            <strong>Dynamic Vehicle Solution</strong>
        </a>
        @else
        <a class="navbar-brand" style="background:red;height:100%; padding:5px;border-radius:2vh" href="{{ url('/') }}">
            <strong>Dynamic Vehicle Solution</strong>
        </a>
        @endif    
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            @if (Auth::check() && Auth::user()->role=='admin')
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle navbar-brand" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="color:white">Admin Controls</a>
              <ul class="dropdown-menu">
                <li>
                    
                    <a class="dropdown-item" href="{{ url('/adminaddvehicle') }}">
                        Add Vehicle
                    </a>
                    
                </li>
                <li>
                   
                    <a class="dropdown-item" href="{{ url('/admininventory') }}">
                        Vehicle Inventory
                    </a>
                   
                </li>
                
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="{{ url('adminincome') }}">Income Management</a></li>
                <li><a class="dropdown-item" href="{{ url('/adminexpense') }}">Expense Management</a></li>
            </ul>
            </li>

            <a class="navbar-brand" href="{{ url('/adminreservation') }}">
                Reservation Management
            </a>
             @endif

            @if (Auth::check())
            <a class="navbar-brand" href="{{ url('/reservation') }}">
                Create Reservation
            </a>
            @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" style="color:white;" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" style="color:white;" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" style="color:green"href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>