<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminaddvehicle');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'vehicle_name' => 'required', 'string', 'max:60',
            'vehicle_category' => 'required',
            'occupants' => 'required',
            'base_price' => 'required',
            'per_km_price' => 'required',
            'vehicle_photo' => 'required',
            'status' => 'required',

        ]);

        $vehicle=new Vehicle();
        $check_vehicle=Vehicle::where('vehicle_name',$request->vehicle_name)->get();
        if($check_vehicle->isEmpty()){
        $fileName = time().'.'.$request->vehicle_photo->extension();  
        $request->vehicle_photo->move(public_path('uploads'), $fileName);
        $vehicle->vehicle_name=$request->vehicle_name;
        $vehicle->vehicle_category=$request->vehicle_category;
        $vehicle->occupants=$request->occupants;
        $vehicle->base_price=$request->base_price;
        $vehicle->per_km_price=$request->per_km_price;
        $vehicle->vehicle_photo="uploads\\\\".$fileName;
        $vehicle->status=$request->vehicle_status;
        $vehicle->save();
        Session::flash('message','Vehicle added successfully'); 
        return redirect('adminaddvehicle');
        }
        else{
        Session::flash('message','This vehicle has already been added');
        return redirect('adminaddvehicle');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
