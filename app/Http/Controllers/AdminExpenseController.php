<?php
namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Expense;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

class AdminExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Expense::all();
        $total=0;
        $recent=0;
        foreach($data as $d){
            $total=$d->sum('amount');
            $recent=$d->amount;
        }
        $data=Expense::latest()->paginate(4);     
        return view('adminexpense',compact('data','total','recent'));
        //return view('adminexpense');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required', 'string', 'max:255',
            'date' => 'required',
            'expense_type' => 'required',
            'amount' => 'required',
        ]);
        $data=new Expense();
        $data->description=$request->description;
        $data->date=$request->date;
        $data->expense_type=$request->expense_type;
        $data->amount=$request->amount;
        $data->save();
        Session::flash('message', 'Expense added.');
        return redirect('adminexpense');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $data=Expense::findOrfail($id);
        $data->description=$request->description;
        $data->date=$request->date;
        $data->expense_type=$request->expense_type;
        $data->amount=$request->amount;
        $data->update();
        Session::flash('message', 'Expense updated.');
        return redirect('adminexpense');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Expense::findOrfail($id);
        // return $data;      
        $data->delete();
        
        return redirect('/adminexpense');
    }
}
