@extends('layouts.app')

@section('content')

<div class="container">
          
    <div class="row">
        <div class="col-lg-4 md-6 sm-6">
        @if (session('message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
            <div class="card" style="padding:5px;">
                <div class="card-header">Create Reservation</div>
                <div class="card-content" style="padding:5px">
                    <form action="reserve" method="POST">
                        @csrf

                        <div class="mb-2">
                            <input type="text" class="form-control" name="pickup_location" value="{{ old('pickup_location') }}" placeholder="Pickup Location">
                            
                                <span style="color:red">
                                    @error('pickup_location')
                                    {{$message}}
                                    @enderror
                                </span>
                            
                        </div>
                        <div class="mb-2">
                            <input type="text" class="form-control" name="drop_location" value="{{ old('drop_location') }}" placeholder="Drop Location">
                                <span style="color:red">
                                    @error('drop_location')
                                    {{$message}}
                                    @enderror
                                </span>
                        </div>

                        <div class="d-flex flex-row mb-2" >
                            <div class="mr-1" style="width:29vh">
                                <input type="date" class="form-control" name="date" value="{{ old('date') }}"placeholder="Date">
                                    <span style="color:red">
                                        @error('date')
                                        {{$message}}
                                        @enderror
                                    </span>
                            </div>

                            <div>
                                <input type="time" class="form-control" name="time" value="{{ old('time') }}" placeholder="Time">
                                    <span style="color:red">
                                        @error('time')
                                        {{$message}}
                                        @enderror
                                    </span>
                            </div>
                        </div>

                        <div class="mb-2">
                            <select class="form-select" name="vehicle" value="{{ old('vehicle') }}">
                            <option selected disabled>Select Vehicle Type</option>
                            @foreach($category as $d)
                                <optgroup label="{{$d->vehicle_category}}">
                                    @foreach($data as $e)
                                        @if($e->vehicle_category==$d->vehicle_category){
                                            <option value="{{$e->vehicle_name}}">{{$e->vehicle_name}}</option>
                                        }
                                        @endif
                                    @endforeach
                                </optgroup>
                            @endforeach
                            </select>
                                <span style="color:red">
                                    @error('vehicle')
                                    {{$message}}
                                    @enderror
                                </span>
                        </div>

                        <div class="mb-2">
                            <input class="form-control" type="text" placeholder="Distance" name="distance" disabled>
                        </div>

                        <div class="mb-2">
                            <input class="form-control" type="text" placeholder="Price" name="price" disabled>
                        </div>

                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <button class="btn btn-primary" type="submit">Create</button>
                            <button class="btn btn-secondary" type="reset" onclick="window.location.reload()">Reset</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-lg-8 md-6 sm-6">
        
            <div id="mapid">
            <iframe
                width="100%"
                height="100%"
                style="border:0"
                loading="lazy"
                allowfullscreen
               src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0NklmqA0-iDswarJeielwX8GqQc66Wlc
                &q=Shanker+Dev+Campus,TU+Office+of+the+Controller+of+Examinations">
                </iframe>

            </div>
            <!-- src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0NklmqA0-iDswarJeielwX8GqQc66Wlc
                &q=Shanker+Dev+Campus,TU+Office+of+the+Controller+of+Examinations"> -->
        </div>
    </div>
</div>


@endsection


@section('scripts')



<script>
//     var mymap = L.map('mapid').setView([27.6892734,85.3233855], 12);
//     L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicHJhamVzaCIsImEiOiJja3F3aDYwN2owOHlnMnVwbHFyYnJyZXA0In0.RgiBjaQXyGZrNQtskQITqg', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     access_token: 'pk.eyJ1IjoicHJhamVzaCIsImEiOiJja3F3aDYwN2owOHlnMnVwbHFyYnJyZXA0In0.RgiBjaQXyGZrNQtskQITqg',
// }).addTo(mymap);

// var marker = L.marker([27.703470931318975, 85.32191744259735]).addTo(mymap);


</script>
@endsection
