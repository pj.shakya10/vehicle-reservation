@extends('layouts.app')

@section('content')
<div class="container">

  @if (session('message')=='Reservation successful.'|| session('message')=='Payment successful.')
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('message') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
  @endif
  @if (session('message')=='Sorry, Confirmed reservation cannot be deleted. Contact@ 01-5555999 to cancel confirmed reservation.' || session('message')=='Payment unsuccessful.')
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('message') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
  @endif
  @if(!$data->isEmpty())
  <div class="card">
    <div class="card-header">Dashboard</div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">Pickup Location</th>
          <th scope="col">Drop Location</th>
          <th scope="col">Date</th>
          <th scope="col">Time</th>
          <th scope="col">Vehicle</th>
          <th scope="col">Price</th>
          <th scope="col">Status</th>
          <th scope="col">Action</th>
        </tr>
      </thead>

      @foreach($data as $d)

      <tbody>

        <tr>
          <th scope="row">{{ $loop->index +1}}</th>
          <td>{{$d->pickup_location}}</td>
          <td>{{$d->drop_location}}</td>
          <td>{{$d->date}}</td>
          <td>{{$d->time}}</td>
          <td>{{$d->vehicle}}</td>
          <td>{{$d->price}}</td>

          @if($d->status=='Confirm')
          <td class="pr-0">

            <form action="https://uat.esewa.com.np/epay/main" method="POST">
              <input value="{{$d->price}}" name="tAmt" type="hidden">
              <input value="{{$d->price}}" name="amt" type="hidden">
              <input value="0" name="txAmt" type="hidden">
              <input value="0" name="psc" type="hidden">
              <input value="0" name="pdc" type="hidden">
              <input value="EPAYTEST" name="scd" type="hidden">
              <input value="{{$d->id}}" name="pid" type="hidden">
              <input value="http://127.0.0.1:8000/payment-verify/{{$d->id}}?q=su" type="hidden" name="su">
              <input value="http://127.0.0.1:8000/payment-verify/{{$d->id}}?q=fu" type="hidden" name="fu">
              <button type="submit" style="background-color:green;color:white;border-radius:2vh">Pay via eSewa</button>
            </form>

          </td>
          @else
          <td>{{$d->status}}</td>
          @endif

          <td>

            <button class="btn btn-danger p-1" onclick="showAlert('{{$d->id}}')">
              <i class="fas fa-trash-alt" style="font-size:23px;"></i>
              <!-- <img src="{{ URL::asset('images/delete.png') }}" width="23px"height="28px"alt="Delete"> -->
            </button>
            <form action="reservation/delete/{{$d->id}}" id="deleteForm/{{$d->id}}" method="POST">
              @csrf
              @method('DELETE')
            </form>

          </td>
        </tr>

      </tbody>
      @endforeach
    </table>
    <div class="d-flex justify-content-center">

      {{$data->links("pagination::bootstrap-4")}}
    </div>
  </div>
  @else

  <div class="row-12">
    <div class="d-flex justify-content-center align-items-center" style="height:80vh;">
      <a href="{{ url('/reservation') }}" style="text-decoration: none; color:white;">
        <button class="btn p-3 shadow-lg " style="background:#FF0000; height:10vh; border-radius:3vh">
          <h2 style="color:white">Create Your Reservation </h2>
        </button>
      </a>
    </div>
  </div>

  @endif
</div>
@endsection

@section('scripts')
<script>
  function showAlert(id) {
    var form = document.getElementById("deleteForm/" + id);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        form.submit();
      } else {
        return false;
      }
    })
  }
</script>
@endsection