@extends('layouts.app')

@section('content')

<div class="container">
         
    <div class="row">
        <div class="col-md-4">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
            <div class="card" style="padding:4px;">
                <div class="card-header">Add Income</div>
                <div class="card-content" style="padding:5px">
                    <form action="adminincome/add" method="POST">
                        @csrf

                        <div class="mb-3">
                            <input type="date" class="form-control" name="date" value="{{ old('date') }}" placeholder="Date">
                            
                                <span style="color:red">
                                    @error('date')
                                    {{$message}}
                                    @enderror
                                </span>
                            
                        </div>
                        <div class="mb-3">
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}" placeholder="Description">
                                <span style="color:red">
                                    @error('description')
                                    {{$message}}
                                    @enderror
                                </span>
                        </div>

                        <div class="mb-3">
                                <select class="form-select" name="income_type" value="{{ old('income_type') }}">
                                    <option selected disabled>Select Income Category</option>
                                    <option value="Esewa ">Esewa</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                    <option value="Other">Other</option>
                                </select>
                            
                                <span style="color:red">
                                    @error('income_type')
                                    {{$message}}
                                    @enderror
                                </span>
                        </div>

                        <div class="mb-3">
                            <input type="number" class="form-control" name="amount" value="{{ old('amount') }}" placeholder="Amount">
                                <span style="color:red">
                                    @error('amount')
                                    {{$message}}
                                    @enderror
                                </span>
                        </div>

                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <button class="btn btn-primary" type="submit">Add</button>
                                <button class="btn btn-secondary" type="reset" onclick="window.location.reload()">Clear</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-8">
            <div class="row" style="padding: 0px 12px 2px 12px">
                <div class="card col-6">
                    <div class="card-header">Total Income</div>
                    <div class="card-content">
                        <h2>NPR.{{$total}}</h2>
                    </div>
                </div>
                <div class="card col-6">
                    <div class="card-header">Recent Income</div>
                    <div class="card-content">
                        <h2>NPR.{{$recent}}</h2>
                    </div>
                </div>
            </div>
            @if(!$data->isEmpty())
            <div class="card" style="margin-top:10px;">
                <div class="card-header">
                    Income
                </div>
                <div class="card-content">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Date</th>
                                <th scope="col">Description</th>
                                <th scope="col">Income Category</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($data as $d)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $loop->index +1}}</th>
                                <td>{{$d->date}}</td>
                                <td>{{$d->description}}</td>
                                <td>{{$d->income_type}}</td>
                                <td>{{$d->amount}}</td>
                                <td>
                                    <div class="d-flex flex-row justify-content-evenly">
                                        <button class="btn btn-primary p-1"  onclick="editIncome('{{$d}}') " >
                                            <i class="fas fa-edit" style="font-size:23px;"></i>
                                            <!-- <img src="{{ URL::asset('images/edit.png') }}" width="23px"height="28px"alt="Edit"> -->
                                        </button>
                                
                                        <button class="btn btn-danger p-1" onclick="showAlert('{{$d->id}}')">
                                            <i class="fas fa-trash-alt" style="font-size:23px;"></i>  
                                            <!-- <img src="{{ URL::asset('images/delete.png') }}" width="23px"height="28px"alt="Delete"> -->
                                        </button>
                                        <form action="adminincome/delete/{{$d->id}}" id="deleteForm/{{$d->id}}" method="POST">
                                            @csrf
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                        
                    </table>
                    <div class="d-flex justify-content-center">
                        {{$data->links("pagination::bootstrap-4")}}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="editModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Income</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form  method="POST" id="updateForm">
                    <div class="modal-body">
                    
                        @csrf

                        <div class="mb-3">
                            <input type="date" class="form-control" name="date" id="date" placeholder="Date" required>
                        </div>

                        <div class="mb-3">
                            <input type="text" class="form-control" name="description" id="description" placeholder="Description" required>
                        </div>

                        <div class="mb-3">
                                <select class="form-select" name="income_type" id="income_type" required>
                                    <option selected disabled>Select Income Category</option>
                                    <option value="Esewa">Esewa</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                    <option value="Other">Other</option>
                                </select>
                            
                        </div>

                        <div class="mb-3">
                            <input type="number" class="form-control" name="amount" id="amount" placeholder="Amount" required>
                                
                        </div>         

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection



@section('scripts')
<script>
function showAlert(id){
    var form = document.getElementById("deleteForm/"+id);
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    form.submit();
  }else{
      return false;
  }
})
}

function editIncome(data){
    const parsedData=JSON.parse(data);   
    $('#updateForm').attr('action', '/adminincome/update/'+parsedData.id);
    $('#date').val(parsedData.date);
    $('#description').val(parsedData.description);
    $('#income_type').val(parsedData.income_type);
    $('#amount').val(parsedData.amount);
    $('#editModal').modal('show');
}
</script>
@endsection
