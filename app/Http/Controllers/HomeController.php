<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data=Reservation::with('user')->where('user_id','=',$request->user()->id)->paginate(8);
        // $data=User::findOrFail($request->user()->id)->with('reservations')->paginate(5);
        // return $data;
        return view('home',compact("data"));
    }
    public function destroy($id)
    {
        //
    }
    
}
