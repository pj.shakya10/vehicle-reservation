@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col">
            @if (session('message')=='Vehicle already added')
                <div class="alert alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    Vehicle Inventory
                </div>
                <div class="card-content">
                    <table class="table table-striped" style="text-align:center">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Vehicle Category</th>
                                <th scope="col">Vehicle Name</th>
                                <th scope="col">Base Price</th>
                                <th scope="col">Per Km Price</th>
                                <th scope="col">Occupants(including driver)</th>
                                <th scope="col">Vehicle Photo</th>
                                <th scope="col">Vehicle Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($data as $d)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $loop->index +1}}</th>
                                <td>{{$d->vehicle_category}}</td>
                                <td>{{$d->vehicle_name}}</td>
                                <td>{{$d->base_price}}</td>
                                <td>{{$d->per_km_price}}</td>
                                <td>{{$d->occupants}}</td>
                                <td><img src="{{$d->vehicle_photo}}" alt="" height="70px" width="80px"></td>
                                <td>{{$d->status}}</td>
                                <td>
                                    <div class="d-flex flex-row justify-content-evenly">
                                        <button class="btn btn-primary p-1"  onclick="datatomodal('{{$d}}')" >
                                            <i class="fas fa-edit" style="font-size:23px;"></i>
                                            <!-- <img src="{{ URL::asset('images/edit.png') }}" width="23px"height="28px"alt="Edit"> -->
                                        </button>
                                    
                                        <button class="btn btn-danger p-1" onclick="showAlert('{{$d->id}}')">
                                            <i class="fas fa-trash-alt" style="font-size:23px;"></i>    
                                            <!-- <img src="{{ URL::asset('images/delete.png') }}" width="23px"height="28px"alt="Delete"> -->
                                        </button>
                                        
                                        <form action="admininventory/delete/{{$d->id}}" id="deleteForm/{{$d->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach


                    </table>
                    <div class="d-flex justify-content-center">
                        {{$data->links("pagination::bootstrap-4")}}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="editModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Inventory</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form  method="POST" id="updateForm" enctype="multipart/form-data">
                    <div class="modal-body">

                            @csrf
                            
                            <div class="mb-3">
                                <input type="text" class="form-control"  name="vehicle_name" placeholder="Vehicle name" id="vehicleName" required>
                            </div>

                            <div class="mb-3">
                                <select class="form-select" name="vehicle_category" id="vehicleCategory" required>
                                    <option selected>Select Vehicle Category</option>
                                    <option value="Hatchback">Hatchback</option>
                                    <option value="Suv">Suv</option>
                                    <option value="Sedan">Sedan</option>
                                    <option value="Bus">Bus</option>
                                    <option value="MiniBus">Minibus</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <input type="number" class="form-control"  name="base_price" placeholder="Base price" id="basePrice" required>
                            </div>

                            <div class="mb-3">
                                <input type="number" class="form-control"  name="per_km_price" placeholder="Per Km price" id="perkmprice" required>
                            </div>

                            <div class="mb-3">
                                <input type="number" class="form-control"  name="occupants" placeholder="Occupants(including driver)" id="occupants" required>
                            </div>

                            <div class="mb-3">
                                <label for="formFile" class="form-label">Vehicle photo</label>
                                <input class="form-control" type="file" name="vehicle_photo" id="vehiclePhoto" >
                            </div>

                            <div class="mb-3">
                            <select class="form-select" name="vehicle_status" id="vehicleStatus" required>
                                <option selected>Select Vehicle Status</option>
                                <option value="Active">Active</option>
                                <option value="Deactive">Deactive</option>  
                            </select>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
function showAlert(id){
    var form = document.getElementById("deleteForm/"+id);
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    form.submit();
  }else{
      return false;
  }
})
}
function datatomodal(data){
    const parsedData=JSON.parse(data);   
    $('#updateForm').attr('action', '/admininventoryupdate/'+parsedData.id);
    $('#vehicleName').val(parsedData.vehicle_name);
    $('#vehicleCategory').val(parsedData.vehicle_category);
    $('#basePrice').val(parsedData.base_price);
    $('#perkmprice').val(parsedData.per_km_price);
    $('#occupants').val(parsedData.occupants);
    $('#vehicleStatus').val(parsedData.status);
    $('#editModal').modal('show');
    
}
</script>
@endsection
