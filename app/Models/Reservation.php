<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = [
        'pickup_location',
        'drop_location',
        'date',
        'time',
        'vehicle',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
