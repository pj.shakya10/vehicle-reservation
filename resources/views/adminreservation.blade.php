@extends('layouts.app')

@section('content')

<div class="container">

  @if (session('message'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('message') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">User Details</div> 
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Name</th>
              <th scope="col">Mobile</th>
              <th scope="col">Email</th>
              <th scope="col">Address</th>
              <th scope="col">Action</th>
            </tr>
          </thead>

          @foreach($data as $d)
          <tbody>

            <tr>
              <th scope="row">{{ $loop->index +1}}</th>
              <td>{{$d->name}}</td>
              <td>{{$d->mobile}}</td>
              <td>{{$d->email}}</td>
              <td>{{$d->address}}</td>
              <td>
                <div class="d-flex flex-row justify-content-evenly">
                  <button class="btn btn-primary p-1" onclick="showreservations('{{$d->reservations}}')">
                    <i class="fas fa-eye" style="font-size:23px;"></i>
                  </button>
                 
                  <button class="btn btn-danger p-1" onclick="showAlertUser('{{$d->id}}')">
                    <i class="fas fa-trash-alt" style="font-size:23px;"></i>  
                    <!-- <img src="{{ URL::asset('images/delete.png') }}" width="23px"height="28px"alt="Delete"> -->
                  </button>
                  <form action="adminreservation/deleteuser/{{$d->id}}" id="deleteUser/{{$d->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                  </form>
                </div>
              </td>
            </tr>

          </tbody>
          @endforeach
        </table>
        <div class="d-flex justify-content-center">
          {{$data->links("pagination::bootstrap-4")}}
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="reservations" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Reservations</h5>
        <button type="button" class="btn-close" onclick="modalClose()"></button>
      </div>
      <div class="model-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Pickup location</th>
              <th scope="col">Drop Location</th>
              <th scope="col">Date</th>
              <th scope="col">Time</th>
              <th scope="col">Price</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>


          <tbody id="tableBody">

          </tbody>

        </table>
        

      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  function showAlertUser(id){
    console.log(id);
    var form = document.getElementById("deleteUser/"+id);
    Swal.fire({
  title: 'Are you sure? This will delete User data as well as their reservation data',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
  if (result.isConfirmed) {
    form.submit();
  }else{
      return false;
  }
  })
  }
 
  function showreservations(data) {
    const parsedData = JSON.parse(data);
    const tableBody = document.getElementById("tableBody");

    parsedData.forEach(function(row, index) {
      
      let tableRow = document.createElement('tr');

      let td_pickup = document.createElement('td');
      td_pickup.innerHTML = row.pickup_location;

      let td_drop = document.createElement('td');
      td_drop.innerHTML = row.drop_location;

      let td_date = document.createElement('td');
      td_date.innerHTML = row.date;

      let td_time = document.createElement('td');
      td_time.innerHTML = row.time;

      let td_price = document.createElement('td');
      td_price.innerHTML = row.price;

      let td_status = document.createElement('td');
      if(row.status!='Paid'){
      td_status.innerHTML = `
                  <form action="adminreservation/update/${row.id}" method="POST" id="update/status/(${row.id})">
                    @csrf
                    <select name="res_status" onchange="form.submit()">
                      <option selected disabled>${row.status}</option>
                      <option value="Confirm">Confirm</option>
                      <option value="Pending">Pending</option>
                      <option value="Pending">Paid</option>
                    </select>
                  </form>`;
      }
      else
      td_status.innerHTML = row.status;
      
      
      let td_action=document.createElement('td');
      td_action.innerHTML=` <button class="btn btn-danger p-1" onclick="showAlert(${row.id})">
                              <i class="fas fa-trash-alt" style="font-size:23px;"></i>  
                            </button>
                            <form action="adminreservation/delete/${row.id}" id="deleteForm/${row.id}" method="POST">
                              @csrf
                              @method('DELETE')
                            </form>`;



      tableRow.appendChild(td_pickup);
      tableRow.appendChild(td_drop);
      tableRow.appendChild(td_date);
      tableRow.appendChild(td_time);
      tableRow.appendChild(td_price);
      tableRow.appendChild(td_status);
      tableRow.appendChild(td_action);
      tableBody.appendChild(tableRow);
      
      
      
    })
    // $('#tablerow').val(parsedData);
    $('#reservations').modal('show');

  }

  function showAlert(id){
    console.log(id);
    var form = document.getElementById("deleteForm/"+id);
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    form.submit();
  }else{
      return false;
  }
})
}

  function modalClose(){
    $("#tableBody tr").remove(); 
    $('#reservations').modal('hide');
  }
</script>


@endsection