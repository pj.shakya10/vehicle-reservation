<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Reservation;
use App\Models\Vehicle;
use SebastianBergmann\Environment\Console;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        //$data=Vehicle::all()->where('status','active')->get()->toArray();
        //return $data;
        $category=Vehicle::where('status','active')->select('vehicle_category')->distinct()->get();
        //return $category;
        $data = Vehicle::where('status','active')->select('vehicle_category','vehicle_name','base_price','per_km_price','vehicle_photo')->distinct()->orderBy('vehicle_category')->get() ;
        //return $data;
        return view('reservation',compact('data','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //dd($request->all());
        $request->validate([
            'pickup_location' => 'required', 'string', 'max:60',
            'drop_location' => 'required', 'string', 'max:60',
            'date' => 'required',
            'time' => 'required',
            'vehicle' => 'required',
        ]);
        
        $reservation=new Reservation();
        $check_reservation=Reservation::where('vehicle',$request->vehicle)->where('date',$request->date)->get();
        if($check_reservation->isEmpty()){
        $reservation->user_id=$request->user()->id;
        $reservation->pickup_location=$request->pickup_location;
        $reservation->drop_location=$request->drop_location;
        $reservation->date=$request->date;
        $reservation->time=$request->time;
        $reservation->vehicle=$request->vehicle;
        $reservation->distance=5;

        // fetch vehicle base price and per km price
        $data=Vehicle::where('vehicle_name',$request->vehicle)->first();
        $base=$data->base_price;
        $per=$data->per_km_price;
        $cost=$base+$per*5;
        
        $reservation->price=$cost;
        $reservation->save();
        Session::flash('message', 'Reservation successful.'); 
        return redirect('home');
        }
        else{
        Session::flash('message','Sorry, Reservation unsuccessful. Preferred Vehicle is already booked.');
        return redirect('reservation');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Reservation::findOrfail($id);
        
        $status=$data->status;
        // return $status;
        if($status=='Pending')
        {
            $data->delete();
            return redirect('/home');
        }
        else{
            Session::flash('message', 'Sorry, Confirmed reservation cannot be deleted. Contact@ 01-5555999 to cancel confirmed reservation.');
            return redirect('home');
        }    
    }
}
