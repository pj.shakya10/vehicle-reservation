<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Reservation;
use App\Models\Income;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Symfony\Component\VarDumper\Cloner\Data;

class EsewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function verifyPayment(Request $request, $id)
    {
        $status=$request->q;
        // dd('i am here');

        $amt=$request->amt;
        $oid=$request->oid;
        $refId=$request->refId;
        // dump($amt,$oid,$refId);

        $data=Reservation::findOrfail($id);
        
        $price=$data->price;
        $id=$data->id;

        if($status=='su'){
            $url = "https://uat.esewa.com.np/epay/transrec";
            $data =[
                'amt'=> $price,
                'rid'=> $refId,
                'pid'=>$id,
                'scd'=> 'EPAYTEST',
            ];
        
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            
            if(strpos($response,'Success')){
                //reservation status update to paid
                $data=Reservation::findOrfail($id);
                $data->status="Paid";
                $data->update();

                //amount added to income table
                $data1=new Income();
                $userdetail=User::findOrfail($data->user_id);
                $description=$data->pickup_location." "."to"." ".$data->drop_location." ".$userdetail->email." ".$data->date." ".$data->vehicle;
                $mytime = Carbon::now('Asia/Kathmandu')->format('Y-m-d');
                
                $data1->description=$description;
                $data1->date=$mytime;
                $data1->income_type="Esewa";
                $data1->amount=$price;
                $data1->save();

                Session::flash('message', 'Payment successful.'); 
                return redirect('home');
            }
            else{
                Session::flash('message', 'Payment unsuccessful.'); 
                return redirect('home');
            }  
        }
        else{
            return redirect('home');
        }
        return redirect('home');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
